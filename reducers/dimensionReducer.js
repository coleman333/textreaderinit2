import {CHANGE_DIMENSIONS} from '../actions/dimensionAction';
import {createReducer} from 'redux-act';

const initialState = {
    width: 0,
    height: 0,
    orientation: 'portrait'
};

export default createReducer({
    [CHANGE_DIMENSIONS]: (state, payload) => {
        return Object.assign({}, state, payload);
    },
}, initialState);