import {combineReducers} from 'redux';
import {routerReducer} from 'react-router-redux';

import userReducer from './userReducer';
import dimensionReducer from './dimensionReducer';
import common from './common';

export default combineReducers({
    router: routerReducer,
    userReducer: userReducer,
    dimension: dimensionReducer,
    common
});
