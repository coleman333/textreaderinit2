import React from 'react';
import {
    StyleSheet,
    Text,
    Button,
    View,
    TextInput,
    TouchableOpacity,
    Platform,
    ScrollView,
    KeyboardAvoidingView,
    Animated,
    Keyboard,
    Dimensions,
    Image,
    Easing,
    TouchableHighlight,
    TouchableWithoutFeedback,
    AsyncStorage
} from 'react-native';

import {connect} from "react-redux";
import userAction from '../actions/userAction';
import {bindActionCreators} from 'redux';
import _ from 'lodash';
import {StackActions, NavigationActions} from 'react-navigation';


const IMAGE_HEIGHT = 408;
const IMAGE_HEIGHT_SMALL = 24;
import Cookie from 'react-native-cookie'


class Settings extends React.Component {

    state = {
        item: 1,
        disAnimate: true,
        menu_expanded: false,
        text: '',
        port: '',
        address: ''
    };

    async componentWillMount() {
        try {
            const port = await AsyncStorage.getItem('port');
            const address = await AsyncStorage.getItem('address');

            this.setState({port: port, address: address});
        } catch (error) {
            console.log(error);
        }
    }

    async confirmText() {
        try {
            const port = _.defaultTo(this.state.port, 3003);
            const address = _.defaultTo(this.state.address, '192.168.0.201');

            if (!_.isNaN(+port)) {
                await AsyncStorage.setItem('port', port);
            }

            if (!_.isEmpty(_.trim(address))) {
                await AsyncStorage.setItem('address', address);
            }

            this.props.navigation.navigate('MainMenu');
        } catch (error) {
            console.log(error);
        }
    }

    render() {
        return (
            <View style={{
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center',
                backgroundColor: 'gray',
                height: "100%"
            }}>
                <View style={{flex: 8, width: '100%', justifyContent: 'center', alignItems: 'center'}}>
                    <ScrollView
                        style={{width: '90%'}}
                    >
                        <View style={{flexDirection: 'column'}}>
                            <View style={styles.ContainerLabel}>
                                <Text style={styles.textLabel}>Введите номер порта</Text>
                            </View>

                            <TextInput
                                multiline={true}
                                numberOfLines={4}
                                blurOnSubmit={false}
                                onChangeText={(port) => this.setState({port})}
                                value={this.state.port}
                                style={styles.textInput}
                            />
                        </View>
                        <View style={{flexDirection: 'column'}}>
                            <View style={styles.ContainerLabel}>
                                <Text style={styles.textLabel}>введите адрес соединения</Text>
                            </View>
                            <TextInput
                                multiline={true}
                                numberOfLines={4}
                                blurOnSubmit={false}
                                onChangeText={(address) => this.setState({address})}
                                value={this.state.address}
                                style={styles.textInput}
                            />
                        </View>
                    </ScrollView>
                </View>
                <View style={{flex: 1}}>
                    <TouchableOpacity onPress={this.confirmText.bind(this)}
                                      style={{
                                          borderWidth: 1,
                                          // borderColor:'rgba(0,0,0,0.2)',
                                          borderColor: 'black',
                                          alignItems: 'center',
                                          justifyContent: 'center',
                                          width: 200,
                                          height: 50,
                                          backgroundColor: 'gray',
                                          borderRadius: 50,
                                      }}
                    ><Text>Подтвердить</Text>
                    </TouchableOpacity>
                </View>

            </View>


        );
    }
}

const styles = StyleSheet.create({
    ContainerLabel: {
        marginTop: 10,
        // height: 50,
        width: '100%',
        // backgroundColor: '#eee',
        // borderWidth:1,
        // borderRadius:25,
        justifyContent: 'center',
    },
    textLabel: {
        textAlign: 'center',
        alignItems: 'center',
    },
    textInput: {
        borderColor: 'black',
        marginTop: 10,
        height: 50,
        width: '100%',
        backgroundColor: '#eee',
        borderWidth: 1,
        borderRadius: 25,
        justifyContent: 'center',
        textAlign: "center"
    }
});

const mapStateToProps = (state) => {
    console.log(state.dimension);
    return {
        // users: state.userReducer.allUsers,
        // gists: state.userReducer.gists
        // user: state.userReducer.login,
        resParseText: state.userReducer.resParseText

    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        userAction: bindActionCreators(userAction, dispatch)
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Settings);
