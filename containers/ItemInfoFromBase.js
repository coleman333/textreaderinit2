import React from 'react';
import {
    StyleSheet,
    Text,
    Button,
    View,
    TextInput,
    TouchableOpacity,
    Platform,
    ScrollView,
    KeyboardAvoidingView,
    Animated,
    Keyboard,
    Image,
    Easing,
    TouchableWithoutFeedback, ActivityIndicator,
} from 'react-native';

import {connect} from "react-redux";
import userAction from '../actions/userAction';
import {bindActionCreators} from 'redux';
import _ from 'lodash';
import BadInstagramCloneApp from './textCamera';

import {StackActions, NavigationActions} from 'react-navigation';


class ItemInfoFromBase extends React.Component {
    spinValue = new Animated.Value(0);
    _animValue2 = new Animated.Value(0);
    state = {
        toScroll: false,
        keyboardHeight: 0,
        normalHeight: 0,
        shortHeight: 0,
        heightOfElementWhenKeyboardOn: '100%',
        item: 1,
        // launchAnimation: 'active',
        facebookName: '',
        isntaToken: '',
        disAnimate: true,
        menu_expanded: false,
        text: '',
        isKeyboardActive:false,
        allItems:[]
    };

    constructor(props) {
        super(props);
        this.myRef = React.createRef();
        this.y_translate = new Animated.Value(0);

    }

    componentWillMount() {
        this.animatedValue1 = new Animated.Value(1);

        if (this.props.navigation.state.params) {
            const {page} = this.props.navigation.state.params;
            if (page === "unAuthorized") {
                alert('Вы не зарегистрированы')
            }
        }
        this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow.bind(this));
        this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide.bind(this));

        this.setState({launchAnimation: 'inActive'})

        // this.onChange=_.debounce(this.onChange.bind(this), 1000);
    }

    componentDidMount() {
        this.props.userAction.getAllItems()
            .then(()=>{
                this.setState({allItems:this.props.allItems})
            });
        Animated.loop(
            Animated.timing(
                this.spinValue,
                {
                    toValue: 1,
                    duration: 3000,
                    // easing: Easing.linear,
                    useNativeDriver: true
                }
            )
        ).start();
    }

    componentWillUnmount() {
        this.keyboardDidShowListener.remove();
        this.keyboardDidHideListener.remove();
    }

    _keyboardDidShow(e) {
        //     this.setState({toScroll: true});
        //     this.setState({shortHeight: Dimensions.get('window').height - e.endCoordinates.height});
        //     this.setState({normalHeight: Dimensions.get('window').height});
        //     this.setState({keyboardHeight: e.endCoordinates.height});
        //     this.setState({heightOfElementWhenKeyboardOn: this.state.shortHeight});
        this.setState({isKeyboardActive:true})
    }

    _keyboardDidHide() {
        //     this.setState({toScroll: false});
        //     this.setState({heightOfElementWhenKeyboardOn: this.state.normalHeight});
        this.setState({isKeyboardActive:false})
    }

    handlePressIn() {
        Animated.spring(this.animatedValue1, {
            toValue: 0.5
        }).start()
    }

    handlePressOut() {
        Animated.spring(this.animatedValue1, {
            duration: 2000,
            toValue: 1,
            friction: 4,
            tension: 40
        }).start()
    }

    engrave(){
        // this.props.navigation.navigate('LaunchTyping');
    }

    render() {
        const animatedStyle = {transform: [{scale: this.animatedValue1}]};
        const {allItems}=this.props;
        const {amountOfActiveProcesses: processing} = this.props;
        // alert(`aaaa=>>> ${typeof allItems} - ${_.size(allItems)} - ${typeof _.get(allItems,1)}`);
        return (
            <View style={{
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center',
                backgroundColor: 'gray',
                height: "100%"
            }}>
                {!processing && <ScrollView>
                    {_.map(allItems,(item, index) => {
                        return<View style={{flex:8,width:'98%',justifyContent:'center',alignItems:'center',
                borderWidth:1,borderColor:'black',borderRadius:10,marginBottom:10,marginTop:5,padding:10}} key={index}>
                    <View style={{flexDirection: 'row',marginBottom:10}}>
                        <View style={{width:"50%",justifyContent: 'flex-start'}}>
                            <View style={styles.ContainerLabel}>
                                <Text style={styles.textLabel}>Номер Документа</Text>
                            </View>
                            <View style={styles.ContainerLabel}>
                                <Text style={styles.textLabel}>Код поставщика</Text>
                            </View>
                            <View style={styles.ContainerLabel}>
                                <Text style={styles.textLabel}>Номер сопр Документа</Text>
                            </View>
                            <View style={styles.ContainerLabel}>
                                <Text style={styles.textLabel}>Заказ на покупку</Text>
                            </View>
                            <View style={styles.ContainerLabel}>
                                <Text style={styles.textLabel}>Серийный код</Text>
                            </View>
                        </View>
                        <View style={{width:"50%",justifyContent: 'flex-end',}}>
                            <View style={styles.ContainerLabelRight}>
                                <Text style={styles.textLabelRight}>{_.get(item,['incomeOrder'],0)}</Text>
                            </View>
                            <View style={styles.ContainerLabelRight}>
                                <Text style={styles.textLabelRight}>{_.get(item,['providerCode'],0)}</Text>
                            </View>
                            <View style={styles.ContainerLabelRight}>
                                <Text style={styles.textLabelRight}>{_.get(item,['guestNumber'],0)}</Text>
                            </View>
                            <View style={styles.ContainerLabelRight}>
                                <Text style={styles.textLabelRight}>{_.get(item,['buyingOrder'],0)}</Text>
                            </View>
                            <View style={styles.ContainerLabelRight}>
                                <Text style={styles.textLabelRight}>{_.get(item,['serialNumber'],0)}</Text>
                            </View>
                        </View>
                    </View>
                    <View style={{flexDirection:'column',width:'100%',alignItems: 'center'}}>
                        <View style={{width:'90%',padding:10}}>
                            <TouchableOpacity onPress={this.engrave.bind(this) }
                                              style={{
                                                  borderWidth: 1,
                                                  // borderColor:'rgba(0,0,0,0.2)',
                                                  borderColor: 'black',
                                                  alignItems: 'center',
                                                  justifyContent: 'center',
                                                  width: '100%',
                                                  height: 50,
                                                  backgroundColor: 'gray',
                                                  borderRadius: 50,
                                                  // boxShadow: '10px 10px 5px black'
                                                  shadowColor: 'black', // IOS
                                                  shadowOffset: { height: 5, width: 5 }, // IOS
                                                  shadowOpacity: 5, // IOS
                                                  shadowRadius:25,
                                                  elevation: 8

                                              }}
                            ><Text>Гравировать</Text>
                            </TouchableOpacity>
                        </View>
                    </View>

                </View>})}
                </ScrollView>}
                {
                    !!processing && <View style={[styles2.container]}>
                        {/*<Text>Идет обработка изображения это может занять несколько секунд</Text>*/}
                        <Text>Идет обработка </Text>

                        <ActivityIndicator size="large" color="#0000ff"/>
                    </View>
                }

            </View>

        );
    }
}

const styles2 = StyleSheet.create({
    container: {
        height:'100%',
        width:'100%',
        justifyContent: 'center',
        position:'absolute',
        alignItems: 'center',
        backgroundColor:'white',
    },

});

const styles =StyleSheet.create({
    ContainerLabel:{
        marginTop:10,
        // height: 50,
        width:'100%',
        justifyContent: 'flex-start',
    },
    ContainerLabelRight:{
        marginTop:10,
        // height: 50,
        width:'100%',
        justifyContent: 'flex-end',
    },
    textLabel:{
        textAlign:'left',
        // alignItems:'center',
    },
    textLabelRight:{
        textAlign:'right',
    },
    textInput:{
        borderColor:'black',
        marginTop:10,
        height: 50,
        width:'100%',
        backgroundColor: '#eee',
        borderWidth:1,
        borderRadius:25,
        justifyContent: 'center',
        textAlign: "center"
    }
});

const animButtonStyle = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    buttonStyle: {

        height: 50,
        // width:100,
        borderRadius: 25,
        backgroundColor: 'black',

    }
})

const mapStateToProps = (state) => {
    console.log(state.dimension);
    return {
        resParseText: state.userReducer.resParseText,
        ScanOfDoc: state.userReducer.ScanOfDoc,
        allItems: state.userReducer.allItems,
        amountOfActiveProcesses: state.common.amountOfActiveProcesses,

    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        userAction: bindActionCreators(userAction, dispatch)
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(ItemInfoFromBase);
